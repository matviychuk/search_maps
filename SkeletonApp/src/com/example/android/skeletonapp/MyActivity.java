package com.example.android.skeletonapp;

import android.content.Intent;
import android.graphics.Canvas;
import android.net.Uri;
import android.widget.Toast;
import com.google.gson.Gson;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.jsoup.Jsoup;
import org.osmdroid.tileprovider.tilesource.ITileSource;
import org.osmdroid.util.GeoPoint;
//import org.osmdroid.bonuspack.overlays.Marker;
import org.osmdroid.views.MapView;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.OverlayItem;
import org.w3c.dom.Document;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class MyActivity extends Activity{

	private ITileSource _ITileSource = null;
	public static MapView mapView ;

    public static ArrayList<OverlayItem> anotherOverlayItemArray;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.a_map);

		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

		mapView = (MapView)findViewById(R.id.mapView);
		mapView.setBuiltInZoomControls(true);
		mapView.setMultiTouchControls(true);

		_ITileSource = new MyTileSource("C9458F2DCB754CEEACC54216C7D1EB0A", null, 2, 13,
				256, "", new String[]{"http://maps.kosmosnimki.ru/TileService.ashx"});


		mapView.setTileSource(_ITileSource);

		GeoPoint move_gpt = new GeoPoint(50.445830, 30.521333);
		mapView.getController().setZoom(9);
		mapView.getController().setCenter(move_gpt);

        Object object = null;
        if (null != object)
            return;
        try {
            object  = sendPost();
//            Toast.makeText(getApplicationContext(),
//                    object.getNonact_markers()[0].toString(), Toast.LENGTH_LONG).show();

            anotherOverlayItemArray = new ArrayList<OverlayItem>();
            for (Object.MarkerElement markerElement : object.getMarkers()){

                    anotherOverlayItemArray.add(new OverlayItem(
                            markerElement.getT(), ""+markerElement.getN(), new GeoPoint(markerElement.getLt(), markerElement.getLn())));

            }
            ItemizedIconOverlay<OverlayItem> anotherItemizedIconOverlay
                    = new ItemizedIconOverlay<OverlayItem>(
                    this, anotherOverlayItemArray, new ItemizedIconOverlay.OnItemGestureListener<OverlayItem>() {
                @Override
                public boolean onItemSingleTapUp(final int i, final OverlayItem overlayItem) {
                    org.jsoup.nodes.Document doc = null;
                    try {
                        String url = "http://www.shukach.com/uk/node/"+ overlayItem.mDescription;
                        doc = Jsoup.connect(url).get();
//                        String title = ;
                        Toast.makeText(getApplicationContext(),
                                doc.title(), Toast.LENGTH_LONG).show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    return false;
                }

                @Override
                public boolean onItemLongPress(final int i, final OverlayItem overlayItem) {
                    String url = "http://www.shukach.com/uk/node/"+ overlayItem.mDescription;
                    Intent intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse(url));
                    startActivity(intent);
                    return false;
                }
            });

            mapView.getOverlays().add(anotherItemizedIconOverlay);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }




    // HTTP POST request
    static Object sendPost() throws Exception {
        String url = "http://www.shukach.com/karta/points_change/";
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(url);
        try{
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
            nameValuePairs.add(new BasicNameValuePair("selected_tags_tids", ""));
            nameValuePairs.add(new BasicNameValuePair("selected_type_tids", ""));
            nameValuePairs.add(new BasicNameValuePair("selected_oblasti", ""));
            nameValuePairs.add(new BasicNameValuePair("selected_types", "blog"));
            nameValuePairs.add(new BasicNameValuePair("problem_check", "0"));
            nameValuePairs.add(new BasicNameValuePair("notactual_check", "0"));
            nameValuePairs.add(new BasicNameValuePair("created_check", "0"));
            nameValuePairs.add(new BasicNameValuePair("notvisited_check", "0"));
            nameValuePairs.add(new BasicNameValuePair("visited_check", "0"));
            nameValuePairs.add(new BasicNameValuePair("country", "ua"));
            nameValuePairs.add(new BasicNameValuePair("type", "blog"));
            post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = client.execute(post);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line = "";
            String respons_str = "";
            while ((line = rd.readLine()) != null) {
//                System.out.println(line);
                respons_str += line;
            }
//            System.out.println(respons_str);
            Object obj_respons = new Gson().fromJson(respons_str, Object.class);
//            System.out.println(obj_respons.getMarkers().length);
            return  obj_respons;
        }catch (Exception e){
            return null;
//            System.out.println(e.getMessage());
        }
    }

}
