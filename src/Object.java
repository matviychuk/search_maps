/**
 * Created by root on 14.09.14.
 */
public class Object {
    private java.lang.Object[] reports;

    public void setReports(java.lang.Object[] reports) {
        this.reports = reports;
    }

    public java.lang.Object[] getReports() {
        return reports;
    }

    private MarkerElement[] markers;

    public void setMarkers(MarkerElement[] markers) {
        this.markers = markers;
    }

    public MarkerElement[] getMarkers() {
        return markers;
    }

    private java.lang.Object[] nonact_markers;

    public void setNonact_markers(java.lang.Object[] nonact_markers) {
        this.nonact_markers = nonact_markers;
    }

    public java.lang.Object[] getNonact_markers() {
        return nonact_markers;
    }

    private Points_active_by_type points_active_by_type;

    public void setPoints_active_by_type(Points_active_by_type points_active_by_type) {
        this.points_active_by_type = points_active_by_type;
    }

    public Points_active_by_type getPoints_active_by_type() {
        return points_active_by_type;
    }
    public class Points_active_by_type {
        private Point point;

        public void setPoint(Point point) {
            this.point = point;
        }

        public Point getPoint() {
            return point;
        }

    }

    public class Point {
        private java.lang.Integer active;

        public void setActive(java.lang.Integer active) {
            this.active = active;
        }

        public java.lang.Integer getActive() {
            return active;
        }

        private java.lang.Integer nonact;

        public void setNonact(java.lang.Integer nonact) {
            this.nonact = nonact;
        }

        public java.lang.Integer getNonact() {
            return nonact;
        }

        private java.lang.Integer active_nonact;

        public void setActive_nonact(java.lang.Integer active_nonact) {
            this.active_nonact = active_nonact;
        }

        public java.lang.Integer getActive_nonact() {
            return active_nonact;
        }

    }

    public class MarkerElement {
        private java.lang.Integer n;

        public void setN(java.lang.Integer n) {
            this.n = n;
        }

        public java.lang.Integer getN() {
            return n;
        }

        private java.lang.Double ln;

        public void setLn(java.lang.Double ln) {
            this.ln = ln;
        }

        public java.lang.Double getLn() {
            return ln;
        }

        private java.lang.String t;

        public void setT(java.lang.String t) {
            this.t = t;
        }

        public java.lang.String getT() {
            return t;
        }

        private java.lang.Double lt;

        public void setLt(java.lang.Double lt) {
            this.lt = lt;
        }

        public java.lang.Double getLt() {
            return lt;
        }

    }

//    String rmt = "";
//    String markername = "";
//    String latitude = "47.953917";
//    String longitude = "37.964933";
//    opts opts = new opts();
//    class opts{
//        String type = "";
//        String nid = "";
//        String uid = "";
//        String tnid = "";
//        String []tids ={};
//        String [] visitedby = {};
//        String [] report_ids = {};
//    }
}
